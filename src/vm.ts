import * as compute from "@pulumi/azure-native/compute";
import * as network from "@pulumi/azure-native/network";
import { snetPrivateAdmin, snetPrivateWorker, snetProtectedRedis } from "./network";
import { resourceGroup, config } from "./common";

import { createNIC, } from "./util/newNetwork";

const vmName = 'vm'
export const pubIp1 = new network.PublicIPAddress(`pubIp-${vmName}-1`, {
    publicIpAddressName: `pubIp-${vmName}-1`,
    resourceGroupName: resourceGroup.name,
    publicIPAllocationMethod: "Static",
    sku: {
        name: "Standard",
    }
})
export const vm1pubIp1 = pubIp1.ipAddress

export const NIC1 = createNIC("vm1", resourceGroup.name, pubIp1.id, snetPrivateAdmin.id )

export const vm1 = new compute.VirtualMachine(`${vmName}-1`, {
    vmName: `${vmName}-1`,
    resourceGroupName: resourceGroup.name,
    hardwareProfile: {
        vmSize: compute.VirtualMachineSizeTypes.Standard_DS1_v2,
    },
    networkProfile: {
        networkInterfaces: [{ id: NIC1.id, }],
    },
    storageProfile: {
        imageReference: {
            offer: "UbuntuServer",
            publisher: "Canonical",
            sku: "16.04-LTS",
            version: "latest",
        },
        osDisk: {
            createOption: compute.DiskCreateOption.FromImage,
            diffDiskSettings: {
                option: "Local",
            },
            name: `${vmName}-osDisk`
        }
    },
    osProfile: {
        adminPassword: config.requireSecret("password"),
        adminUsername: "sshuser",
        computerName: `${vmName}-1`,
    },
})



const pubIp2 = new network.PublicIPAddress(`pubIp-${vmName}-2`, {
    publicIpAddressName: `pubIp-${vmName}-2`,
    resourceGroupName: resourceGroup.name,
    publicIPAllocationMethod: "Static",
    sku: {
        name: "Standard",
    }
})

export const vm2pubIp2 = pubIp2.ipAddress
export const NIC2 = createNIC("vm2", resourceGroup.name, pubIp2.id, snetPrivateWorker.id )

export const vm2 = new compute.VirtualMachine(`${vmName}-2`, {
    vmName: `${vmName}-2`,
    resourceGroupName: resourceGroup.name,
    hardwareProfile: {
        vmSize: compute.VirtualMachineSizeTypes.Standard_DS1_v2,
    },
    networkProfile: {
        networkInterfaces: [{ id: NIC2.id, }],
    },
    storageProfile: {
        imageReference: {
            offer: "UbuntuServer",
            publisher: "Canonical",
            sku: "16.04-LTS",
            version: "latest",
        },
        osDisk: {
            createOption: compute.DiskCreateOption.FromImage,
            diffDiskSettings: {
                option: "Local",
            },
            name: `${vmName}-2-osDisk`
        }
    },
    osProfile: {
        adminPassword: config.requireSecret("password"),
        adminUsername: "sshuser",
        computerName: `${vmName}-2`,
    },
})

const pubIp3 = new network.PublicIPAddress(`pubIp-${vmName}-3`, {
    publicIpAddressName: `pubIp-${vmName}-3`,
    resourceGroupName: resourceGroup.name,
    publicIPAllocationMethod: "Static",
    sku: {
        name: "Standard",
    }
})

export const vm3pubIp3 = pubIp3.ipAddress
export const NIC3 = createNIC("vm3", resourceGroup.name, pubIp3.id, snetProtectedRedis.id )

export const vm3 = new compute.VirtualMachine(`${vmName}-3`, {
    vmName: `${vmName}-3`,
    resourceGroupName: resourceGroup.name,
    hardwareProfile: {
        vmSize: compute.VirtualMachineSizeTypes.Standard_DS1_v2,
    },
    networkProfile: {
        networkInterfaces: [{ id: NIC3.id, }],
    },
    storageProfile: {
        imageReference: {
            offer: "UbuntuServer",
            publisher: "Canonical",
            sku: "16.04-LTS",
            version: "latest",
        },
        osDisk: {
            createOption: compute.DiskCreateOption.FromImage,
            diffDiskSettings: {
                option: "Local",
            },
            name: `${vmName}-3-osDisk`
        }
    },
    osProfile: {
        adminPassword: config.requireSecret("password"),
        adminUsername: "sshuser",
        computerName: `${vmName}-3`,
    },
})