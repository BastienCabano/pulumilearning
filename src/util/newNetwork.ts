import * as network from "@pulumi/azure-native/network";
import * as pulumi from "@pulumi/pulumi";

export function createSubnet(name: string,
    rgname: pulumi.Output<string>, 
    vnetname: pulumi.Output<string>, 
    nsgId: pulumi.Output<string>, 
    address: string ) : network.Subnet {

    const subnet = new network.Subnet(name, {
        subnetName: name,
        resourceGroupName: rgname,
        virtualNetworkName: vnetname,
        addressPrefix: address,
        networkSecurityGroup: { id: nsgId, },
    })

    return subnet;
}

export function createSubnetEndPoint(name: string,
    rgname: pulumi.Output<string>, 
    vnetname: pulumi.Output<string>, 
    nsgId: pulumi.Output<string>, 
    address: string,
    endPoints: string ) : network.Subnet {

    const subnet = new network.Subnet(name, {
        subnetName: name,
        resourceGroupName: rgname,
        virtualNetworkName: vnetname,
        addressPrefix: address,
        networkSecurityGroup: { id: nsgId, },
        serviceEndpoints: [{
            service: endPoints,
          },],
    })

    return subnet;
}

export function nsgRule(name: string, rgName: pulumi.Output<string>, NSGname: string, access: string, direction: string, protocol: string, priority: number, srcAd: string, srcPort: string, dstAd: string, dstPort: string) {
    const rule = new network.SecurityRule(name, {
        name: name,
        resourceGroupName: rgName,
        networkSecurityGroupName: NSGname,
        access: access,
        direction: direction,
        protocol: protocol,
        priority: priority,
        sourceAddressPrefix: srcAd,
        sourcePortRange: srcPort,
        destinationAddressPrefix: dstAd,
        destinationPortRange: dstPort,
    })
    return rule;
}

export function createNIC(name: string, rgname: pulumi.Output<string>, pubIpId: pulumi.Output<string>, subnetId: pulumi.Output<string>) {
    const NIC = new network.NetworkInterface(`NIC-${name}`,  {
        networkInterfaceName: `NIC-${name}`,
        resourceGroupName: rgname,
        ipConfigurations: [{
            name: "default",
            publicIPAddress: {
                id: pubIpId,
            },
            subnet: {
                id: subnetId,
            },
        }],
    })

    return NIC;
}
