import * as azure_native from "@pulumi/azure-native";
import * as pulumi from "@pulumi/pulumi";
import { resourceGroup } from "./common";

export const config = new pulumi.Config("db");

/***/
/* Create postgresql database and its firerules
/***/
export const server = new azure_native.dbforpostgresql.Server("server", {
    resourceGroupName: resourceGroup.name,
    serverName: "serverPbDbTest",
    location: "westus",
    properties: {
        administratorLogin: "bacabano",
        administratorLoginPassword: config.requireSecret("password"),
        createMode: "Default",
        minimalTlsVersion: "TLS1_2",
        sslEnforcement: "Enabled",
        storageProfile: {
            backupRetentionDays: 7,
            geoRedundantBackup: "Disabled",
            storageMB: 128000,
        },
    },
    sku: {
        capacity: 2,
        family: "Gen5",
        name: "B_Gen5_2",
        tier: "Basic",
    },
});

export const db1 = new azure_native.dbforpostgresql.Database("db1test", {
    charset: "UTF8",
    collation: "English_United States.1252",
    databaseName: "db1test",
    resourceGroupName: resourceGroup.name,
    serverName: server.name,
});

function firewallRuleSingle(ip: string, name: string) {
    new azure_native.dbforpostgresql.FirewallRule(`firewallRule-${name}`, {
        endIpAddress: ip,
        startIpAddress: ip,
        firewallRuleName: `firewallRule-${name}`,
        resourceGroupName: resourceGroup.name,
        serverName: server.name,
    });
}

firewallRuleSingle("20.71.255.2", "1")
firewallRuleSingle("78.232.75.54", "2")

/***/
/* Create cache redis database
/***/
// const redis = new azure_native.cache.Redis("redis", {
//     resourceGroupName: resourceGroup.name,
//     name: "redisDB",
//     enableNonSslPort: true,
//     location: "West US",
//     minimumTlsVersion: "1.2",
//     redisConfiguration: {
//         "maxmemory-policy": "allkeys-lru",
//     },
//     replicasPerMaster: 2,
//     shardCount: 2,
//     sku: {
//         capacity: 1,
//         name: "Standard",
//         family: "C", //NOT WORKING ??????? family doesn't exist
//     },
//     staticIP: "10.0.2.1",
//     subnetId: "/subscriptions/6bf29401-74a7-45eb-a096-f79ee7952d4c/resourceGroups/rgname/providers/Microsoft.Network/virtualNetworks/vnet-lab/subnets/snet-protected-redis",
//     zones: ["1"],
// })