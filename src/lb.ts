// import * as network from "@pulumi/azure-native/network";
// import { resourceGroup } from "./common";

// export const public_ip_lb = new network.PublicIPAddress(`public_ip_lb`, {
//     publicIpAddressName: `public_ip_lb`,
//     resourceGroupName: resourceGroup.name,
//     publicIPAllocationMethod: "Static",
//     sku: {
//         name: "Standard",
//     }
// })

// export const lb = new network.LoadBalancer("lbForTesting", {
//     resourceGroupName: resourceGroup.name,
//     backendAddressPools: [{
//         name: "be-lb",
//         loadBalancerBackendAddresses: [{
//             virtualNetwork: {id:"/subscriptions/6bf29401-74a7-45eb-a096-f79ee7952d4c/resourceGroups/rgname/providers/Microsoft.Network/virtualNetworks/vnet-lab"},
//             ipAddress: "10.0.0.0/16",
//             name: "regional-lb1-address",
//         }],
//     }],
//     frontendIPConfigurations: [{
//         name: "fe-lb",
//         publicIPAddress: {id: "/subscriptions/6bf29401-74a7-45eb-a096-f79ee7952d4c/resourceGroups/rgname/providers/Microsoft.Network/publicIPAddresses/public_ip_lb",},
    
//     }],
//     probes: [{
//         intervalInSeconds: 15,
//         name: "probe-lb",
//         numberOfProbes: 2,
//         port: 80,
//         protocol: "TCP",
//     }],
//     sku: {
//         name: "Standard",
//     }
//     }, {
//         deleteBeforeReplace: true,
// });