import * as pulumi from "@pulumi/pulumi"
import * as resources from "@pulumi/azure-native/resources"
import * as storage from "@pulumi/azure-native/storage"
import { snetPrivateAdmin, vnet } from "./network"

export const config = new pulumi.Config("iaas")
// Create an Azure Resource Group
export const resourceGroup = pulumi.output(
  resources.getResourceGroup({ resourceGroupName: "rgname" })
)

export const storageAccount = new storage.StorageAccount("sa", {
  resourceGroupName: resourceGroup.name,
  sku: {
      name: storage.SkuName.Standard_LRS,
  },
  kind: storage.Kind.StorageV2,
  tags: {
    project: 'lab',
    session: '2021-04' 
  },
  networkRuleSet: {
      ipRules: [
      // {iPAddressOrRange: "78.232.75.54", action: "Allow"},
      ],
      virtualNetworkRules: [
        {virtualNetworkResourceId: "/subscriptions/6bf29401-74a7-45eb-a096-f79ee7952d4c/resourceGroups/rgname/providers/Microsoft.Network/virtualNetworks/vnet-lab/subnets/snet-private-admin", action: "Allow"},
      ],
      defaultAction: "Deny",
  },
});

export const blobContainer = new storage.BlobContainer("assets", {
  accountName: storageAccount.name,
  containerName: "assets",
  resourceGroupName: resourceGroup.name,
  publicAccess: "Container",
});

const storageAccountKeys = pulumi.all([resourceGroup.name, storageAccount.name]).apply(([resourceGroupName, accountName]) =>
    storage.listStorageAccountKeys({ resourceGroupName, accountName }));
export const primaryStorageKey = storageAccountKeys.keys[0].value;
