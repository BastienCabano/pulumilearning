import * as pulumi from "@pulumi/pulumi";
import * as storage from "@pulumi/azure-native/storage";
import { resourceGroup, storageAccount, blobContainer, config }  from "./common"

// for (let i = 1; i <= 3; i++) {
//     new storage.Blob(`loop${i}.txt`, {
//         accountName: storageAccount.name,
//         resourceGroupName: resourceGroup.name,
//         containerName: blobContainer.name,
//         blobName: `loop${i}.txt`,
//         source: new pulumi.asset.StringAsset(`lab azure - loop ${i}`),
//       })
//   }
  
//   // let fileAsset = new pulumi.asset.FileAsset("./src/coucou.txt");
//   // export const blobFile = new storage.Blob("blobV2", {
//   //     accountName: storageAccount.name,
//   //     containerName: blobContainer.name,
//   //     resourceGroupName: resourceGroup.name,
//   //     blobName: "coucou.txt",
//   //     source: fileAsset,
//   // });

// export const secretFileName = config.requireSecret("secretFileName");
// export const secretFileContent = config.requireSecret("secretFileContent");

// export const blobFile2 = new storage.Blob("secretFile", {
//     accountName: storageAccount.name,
//     containerName: blobContainer.name,
//     resourceGroupName: resourceGroup.name,
//     blobName: secretFileName,
//     source: secretFileContent.apply(
//         (fileContent) => new pulumi.asset.StringAsset(fileContent)
//       ),
// });
