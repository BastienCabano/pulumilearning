import * as network from "@pulumi/azure-native/network";

import { resourceGroup } from "./common";
import { createSubnet, createSubnetEndPoint, nsgRule } from "./util/newNetwork";


export const vnet = new network.VirtualNetwork("vnet-lab", {
    virtualNetworkName: "vnet-lab",
    resourceGroupName: resourceGroup.name,
    addressSpace: {
        addressPrefixes: ["10.0.0.0/16"],
        }
} , {
    ignoreChanges: ['subnets'],
})

const NSG = new network.NetworkSecurityGroup("NSG", {
    resourceGroupName: resourceGroup.name,
    networkSecurityGroupName: "NSG",
})

nsgRule("allow-ssh", resourceGroup.name, "NSG", "Allow", "Inbound", "Tcp", 200, "*", "*", "*", "22" );
nsgRule("allow-ICMP", resourceGroup.name, "NSG", "Allow", "Inbound", "*", 100, "*", "*", "*", "0" );

const NSGworker = new network.NetworkSecurityGroup("NSGworker", {
    resourceGroupName: resourceGroup.name,
    networkSecurityGroupName: "NSGworker",
})

nsgRule("allow-ssh-worker", resourceGroup.name, "NSGworker", "Allow", "Inbound", "Tcp", 200, "*", "*", "*", "22" );
nsgRule("allow-ICMP-worker", resourceGroup.name, "NSGworker", "Allow", "Inbound", "*", 101, "*", "*", "*", "0" );
nsgRule("private-Link-worker", resourceGroup.name, "NSGworker", "Allow", "Outbound", "*", 102, "*", "*", "20.71.255.6", "*" );
nsgRule("deny-Internet-worker", resourceGroup.name, "NSGworker", "Deny", "Outbound", "*", 4096, "*", "*", "Internet", "*" );


const NSGredis = new network.NetworkSecurityGroup("NSGredis", {
    resourceGroupName: resourceGroup.name,
    networkSecurityGroupName: "NSGredis",
})

export const snetPrivateAdmin = createSubnetEndPoint("snet-private-admin", resourceGroup.name, vnet.name, NSG.id, "10.0.0.0/24", 'Microsoft.Storage', )
export const snetPrivateWorker = createSubnet("snet-private-worker", resourceGroup.name, vnet.name, NSGworker.id, "10.0.1.0/24",  )
export const snetProtectedRedis = createSubnetEndPoint("snet-protected-redis", resourceGroup.name, vnet.name, NSGredis.id, "10.0.2.0/24", 'Microsoft.Storage' )


