import * as pulumi from "@pulumi/pulumi";
import * as common from './src/common';
import * as blob from "./src/blob";
import * as vm from "./src/vm";

// import { blobFile } from "./src/blob";
import { server } from "./src/db";
// import { lb } from "./src/lb";


export const primaryStorageKey = common.primaryStorageKey
export const vmIpAddress1 = vm.vm1pubIp1
export const vmIpAddress2 = vm.vm2pubIp2

// export const secretFileContent = blob.secretFileContent
// export const secretFileName = blob.secretFileName
//export const blobFileUrl = {file1: pulumi.interpolate`URL du 1er fichier: ${blobFile.url}`}

export const dbName = {Database: pulumi.interpolate`db: ${server.name}`}

// export const lbId = {LoadBalancerID: pulumi.interpolate`db: ${lb.id}`}